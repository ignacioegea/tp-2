#include <iostream>
#include "Vehiculo.h"

Vehiculo::Vehiculo(int r, bool baul, float c)
{
    this->cantRuedas = r;
    this->cilindrada = c;
    this->tieneBaul = baul;
}

Vehiculo::~Vehiculo()
{
    //dtor
}

void Vehiculo::mostrarEnPantalla()
{
    StackableObject::mostrarEnPantalla();
    std::cout << "Ruedas: " << this->cantRuedas << std::endl;
    std::cout << "Cilind: " << this->cilindrada << std::endl;
    std::cout << "Baul  : " << this->tieneBaul << std::endl << std::endl;

}
