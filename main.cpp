#include <iostream>

#include <StackInterface.h>
#include <StackableObject.h>
#include <FixedArrayStack.h>
#include <Vehiculo.h>

using namespace std;

void cargarPila(StackInterface * stack)
{
    StackableObject * o = new Vehiculo(4, false, 1.0);
    if(!stack->push(o))
    {
        std::cout << "Error haciendo push del elemento 1" << std::endl;
    }

    o = new Vehiculo(2, false, 2.0);
    if(!stack->push(o))
    {
        std::cout << "Error haciendo push del elemento 2" << std::endl;
    }

    o = new Vehiculo(4, true, 3.0);
    if(!stack->push(o))
    {
        std::cout << "Error haciendo push del elemento 3" << std::endl;
    }

    o = new Vehiculo(4, true, 4.0);
    if(!stack->push(o))
    {
        std::cout << "Error haciendo push del elemento 4" << std::endl;
    }

    o = new Vehiculo(4, true, 5.0);
    if(!stack->push(o))
    {
        std::cout << "Error haciendo push del elemento 5" << std::endl;
    }

    o = new Vehiculo(4, true, 6.0);
    if(!stack->push(o))
    {
        std::cout << "Error haciendo push del elemento 6" << std::endl;
    }
}

void imprimirPila(StackInterface * stack)
{
    StackableObject * o;

    while( (o = stack->pop()) != NULL)
    {
        o->mostrarEnPantalla();
    }

    int n = stack->getCount();
    std::cout << "En la pila quedan: " << n << " elementos." << std::endl;
}


int main()
{

    FixedArrayStack stack;

    cargarPila(&stack);
    imprimirPila(&stack);

    return 0;
}
