#ifndef STACKINTERFACE_H
#define STACKINTERFACE_H

#include <StackableObject.h>

class StackInterface
{
    public:
        virtual ~StackInterface();

        virtual StackableObject * pop() = 0;
        virtual bool push(StackableObject *o) = 0;
        virtual int getCount() = 0;

    protected:
        StackInterface();

    private:
};

#endif // STACKINTERFACE_H
