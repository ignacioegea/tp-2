#ifndef DYNAMICMEMSTACK_H
#define DYNAMICMEMSTACK_H

#include <StackInterface.h>


class DynamicMemStack : public StackInterface
{
    public:
        DynamicMemStack();
        virtual ~DynamicMemStack();

    protected:

    private:
};

#endif // DYNAMICMEMSTACK_H
