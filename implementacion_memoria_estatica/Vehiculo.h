#ifndef VEHICULO_H
#define VEHICULO_H

#include <StackableObject.h>


class Vehiculo : public StackableObject
{
    public:
        Vehiculo(int r, bool baul, float c);
        virtual ~Vehiculo();
        int GetcantRuedas() { return cantRuedas; }
        void SetcantRuedas(int val) { cantRuedas = val; }
        bool GettieneBaul() { return tieneBaul; }
        void SettieneBaul(bool val) { tieneBaul = val; }
        float Getcilindrada() { return cilindrada; }
        void Setcilindrada(float val) { cilindrada = val; }
        void mostrarEnPantalla();

    protected:
    private:
        int cantRuedas;
        bool tieneBaul;
        float cilindrada;
};

#endif // VEHICULO_H
