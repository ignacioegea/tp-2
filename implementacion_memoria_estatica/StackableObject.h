#ifndef STACKABLEOBJECT_H
#define STACKABLEOBJECT_H


class StackableObject
{
    public:
        virtual ~StackableObject();
        virtual void mostrarEnPantalla();

    protected:
        StackableObject();

    private:
};

#endif // STACKABLEOBJECT_H
